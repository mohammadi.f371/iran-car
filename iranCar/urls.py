from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/users/', include('users.urls'), name='users'),
    path('api/v1/plans/', include('plans.urls'), name='plans'),
]
