from django.contrib import admin

from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['name', 'family', 'national_code', 'id_number', 'date_joined', 'data_updated']
    ordering = ['-date_joined']


admin.site.register(User, UserAdmin)
