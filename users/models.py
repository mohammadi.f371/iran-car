from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class Gender(models.TextChoices):
    Female = ('Female', _('خانم'))
    Men = ('Men', _('آقا'))


class level_education(models.TextChoices):
    Illiterate = ('Illiterate', _('بی سواد'))
    High_school = ('High_school', _('ابتدایی'))
    Diploma = ('Diploma', _('دیپلم'))
    Bachelor = ('Bachelor', _('کارشناسی'))
    Master = ('Master', _('کارشناسی ارشد'))
    PHD = ('PHD', _('دکتری'))


class marital_status(models.TextChoices):
    Single = ('Single', _('مجرد'))
    Married = ('Married', _('متاهل'))
    Other = ('Other', _('سایر'))


class Province(models.TextChoices):
    AzarbaijanSharghi = ('AzarbaijanSharghi', _('آذربایجان شرقی'))
    AzarbaijanGharbi = ('AzarbaijanGharbi', _('آذربایجان غربی'))
    Ardebil = ('Ardebil', _('اردبیل'))
    Isfahan = ('is', _('اصفهان'))
    Alborz = ('Alborz', _('البرز'))
    Ilam = ('Ilam', _('ایلام'))
    Bushehr = ('Bushehr', _('بوشهر'))
    Tehran = ('Tehran', _('تهران'))
    ChaharmahalBakhtiari = ('ChaharmahalBakhtiari', _('چهارمحال و بختیاری'))
    SouthKhorasan = ('SouthKhorasan', _('خراسان جنوبی'))
    RazaviKhorasan = ('RazaviKhorasan', _('خراسان رضوی'))
    NorthKhorasan = ('NorthKhorasan', _('خراسان شمالی'))
    Khuzestan = ('Khuzestan', _('خوزستان'))
    Zanjan = ('Zanjan', _('زنجان'))
    Semnan = ('Semnan', _('سمنان'))
    SistanBaluchestan = ('SistanBaluchestan', _('سیستان و بلوچستان'))
    Fars = ('Fars', _('فارس'))
    Qazvin = ('Qazvin', _('قزوین'))
    Qom = ('Qom', _('قم'))
    Kurdistan = ('Kurdistan', _('کردستان'))
    Kerman = ('Kerman', _('کرمان'))
    Kermanshah = ('Kermanshah', _('کرمانشاه'))
    KohgiluyehBoyerAhmad = ('KohgiluyehBoyerAhmad', _('کهگیلویه و بویراحمد'))
    Golestan = ('Golestan', _('گلستان'))
    Lorestan = ('Lorestan', _('لرستان'))
    Gilan = ('Gilan', _('گیلان'))
    Mazandaran = ('Mazandaran', _('مازندران'))
    Markazi = ('Markazi', _('مرکزی'))
    Hormozgan = ('Hormozgan', _('هرمزگان'))
    Hamedan = ('Hamedan', _('همدان'))
    Yazd = ('Yazd', _('یزد'))


class User(AbstractUser):
    is_complete_register = models.BooleanField(default=False)
    national_code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=255)
    family = models.CharField(max_length=255)
    father_name = models.CharField(max_length=255)
    id_number = models.CharField(max_length=255)
    serial_number = models.CharField(max_length=255)
    sex = models.CharField(max_length=255, choices=Gender.choices)
    marital_status = models.CharField(max_length=255, choices=marital_status.choices)
    level_education = models.CharField(max_length=255, choices=level_education.choices)
    job = models.CharField(max_length=255)

    birthday_province = models.CharField(max_length=255, choices=Province.choices)
    birthday_city = models.CharField(max_length=255)
    register_province = models.CharField(max_length=255, choices=Province.choices)
    register_city = models.CharField(max_length=255)
    birthday_date = models.DateField(null=True)
    birthday_date_register = models.DateField(null=True)

    residence_province = models.CharField(max_length=255, choices=Province.choices)
    residence_city = models.CharField(max_length=255)
    main_street = models.CharField(max_length=255)
    auxiliary_street = models.CharField(max_length=255)
    alley = models.CharField(max_length=255)
    floor = models.IntegerField(null=True)
    unit = models.IntegerField(null=True)
    number_plates = models.IntegerField(null=True)
    postal_code = models.CharField(max_length=255)
    region = models.IntegerField(null=True)
    phone = models.CharField(max_length=12)
    mobile = models.CharField(unique=True, max_length=12)
    email = models.EmailField(unique=True)

    work_city = models.CharField(max_length=255)
    work_province = models.CharField(max_length=255, choices=Province.choices)
    work_phone = models.CharField(max_length=15)
    work_address = models.TextField()

    driver_license_number = models.CharField(max_length=255, unique=True, null=True)
    shaba_number = models.CharField(max_length=255, unique=True)

    data_updated = models.DateTimeField(auto_now=True)
    USERNAME_FIELD = 'national_code'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.name
