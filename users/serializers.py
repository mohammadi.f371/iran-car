from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from rest_framework import serializers
from .models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['national_code'] = user.national_code
        return token


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('national_code', 'password', 'password2', 'email', 'mobile', 'username')
        extra_kwargs = {
            'mobile': {'required': True},
            'national_code': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            national_code=validated_data['national_code'],
            email=validated_data['email'],
            mobil=validated_data['mobil'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class UpdateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (

            'name',
            'family',
            'father_name',
            'id_number',
            'serial_number',
            'sex',
            'marital_status',
            'level_education',
            'job',
            'birthday_province',
            'birthday_city',
            'register_province',
            'register_city',
            'birthday_date',
            'birthday_date_register',
            'residence_province',
            'residence_city',
            'main_street',
            'auxiliary_street',
            'alley',
            'floor',
            'unit',
            'number_plates',
            'postal_code',
            'region',
            'phone',
            'work_city',
            'work_province',
            'work_phone',
            'work_address',
            'driver_license_number',
            'shaba_number',
        )
        extra_kwargs = {
        }

    def validate_email(self, value):
        user = self.context['request'].user
        if User.objects.exclude(pk=user.pk).filter(email=value).exists():
            raise serializers.ValidationError({"email": "This email is already in use."})
        return value

    def validate_username(self, value):
        user = self.context['request'].user
        if User.objects.exclude(pk=user.pk).filter(national_code=value).exists():
            raise serializers.ValidationError({"national_code": "This national_code is already in use."})
        return value

    def update(self, instance, validated_data):
        instance.name = validated_data['name']
        instance.family = validated_data['family']
        instance.father_name = validated_data['father_name']
        instance.id_number = validated_data['id_number']
        instance.serial_number = validated_data['serial_number']
        instance.sex = validated_data['sex']
        instance.marital_status = validated_data['marital_status']
        instance.level_education = validated_data['level_education']
        instance.job = validated_data['job']
        instance.birthday_province = validated_data['birthday_province']
        instance.birthday_city = validated_data['birthday_city']
        instance.register_province = validated_data['register_province']
        instance.register_city = validated_data['register_city']
        instance.birthday_date = validated_data['birthday_date']
        instance.birthday_date_register = validated_data['birthday_date_register']
        instance.residence_province = validated_data['residence_province']
        instance.residence_city = validated_data['residence_city']
        instance.main_street = validated_data['main_street']
        instance.auxiliary_street = validated_data['auxiliary_street']
        instance.alley = validated_data['alley']
        instance.floor = validated_data['floor']
        instance.unit = validated_data['unit']
        instance.number_plates = validated_data['number_plates']
        instance.postal_code = validated_data['postal_code']
        instance.region = validated_data['region']
        instance.phone = validated_data['phone']
        instance.work_city = validated_data['work_city']
        instance.work_province = validated_data['work_province']
        instance.work_phone = validated_data['work_phone']
        instance.work_address = validated_data['work_address']
        instance.driver_license_number = validated_data['driver_license_number']
        instance.shaba_number = validated_data['shaba_number']
        instance.is_complete_register = True
        instance.save()
        return instance
