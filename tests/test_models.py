from django.test import TestCase
from plans.models import Car


class CarModelsTest(TestCase):
    def setUp(self):
        Car.objects.create(name='name', company='company', exterior_color='exterior_color',
                           interior_color='interior_color', code_product='code_product',
                           model_product='model_product', fuel_type='fuel_type',
                           features='features', account_type='account_type')

    def test_car_content(self):
        car = Car.objects.get(id=1)
        self.assertQuerysetEqual(f'{car.name}', 'name')
        self.assertQuerysetEqual(f'{car.company}', 'company')
        self.assertQuerysetEqual(f'{car.exterior_color}', 'exterior_color')
        self.assertQuerysetEqual(f'{car.interior_color}', 'interior_color')
        self.assertQuerysetEqual(f'{car.code_product}', 'code_product')
        self.assertQuerysetEqual(f'{car.model_product}', 'model_product')
        self.assertQuerysetEqual(f'{car.features}', 'features')
        self.assertQuerysetEqual(f'{car.fuel_type}', 'fuel_type')
        self.assertQuerysetEqual(f'{car.account_type}', 'account_type')
