from django.urls import reverse
from rest_framework.test import APITestCase

from plans.models import Car


class CarViewTest(APITestCase):
    @classmethod
    def setUpTestData(cls):
        number_of_authors = 13

        for author_id in range(number_of_authors):
            Car.objects.create(
                name=f'Car {author_id}',
                company=f'Saipa {author_id}',
                exterior_color='black',
                interior_color='black',
                code_product=f'SP{author_id}',
                model_product='2022',
                fuel_type='Gaz',
                features='no thing',
                account_type='user',
            )

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('api/v1/plans/car')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('car-url'))
        self.assertEqual(response.status_code, 200)
