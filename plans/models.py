from django.db import models
from users.models import User


class Car(models.Model):
    name = models.CharField(max_length=255)
    company = models.CharField(max_length=255)
    exterior_color = models.CharField(max_length=255)
    interior_color = models.CharField(max_length=255)
    code_product = models.CharField(max_length=255, unique=True)
    model_product = models.CharField(max_length=255)
    fuel_type = models.CharField(max_length=255)
    features = models.CharField(max_length=255)
    account_type = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Plan(models.Model):
    type_plan = models.CharField(max_length=255)
    deliver_time = models.IntegerField()
    insurance = models.BooleanField(default=False)
    name_insurance = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    date = models.DateField(null=True)
    expired = models.IntegerField()
    lottery = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class PlanCar(models.Model):
    car = models.ForeignKey(Car, on_delete=models.SET_NULL, null=True)
    plan = models.ForeignKey(Plan, on_delete=models.SET_NULL, null=True)
    cost = models.CharField(max_length=255)
    Amount_on_account = models.CharField(max_length=255)
    number = models.IntegerField()

    def __str__(self):
        return self.plan.name + ' ' + self.car.name

    class Meta:
        db_table = 'PlanCar'
        constraints = [
            models.UniqueConstraint(fields=['car', 'plan'], name='unique plan')
        ]


class UserPlan(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    PlanCar = models.ForeignKey(PlanCar, on_delete=models.SET_NULL, null=True)
    win = models.BooleanField(default=False)

    def __str__(self):
        return self.user.name

    class Meta:
        db_table = 'UserPlan'
        constraints = [
            models.UniqueConstraint(fields=['user', 'PlanCar'], name='unique_plan_user')
        ]

    def save(self, *args, **kwargs):
        self.plan = self.PlanCar.plan
        super(UserPlan, self).save(*args, **kwargs)
