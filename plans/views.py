from rest_framework.permissions import IsAuthenticated
from rest_framework import generics

from .models import Car,Plan,PlanCar
from .serializers import CarSerializer, PlanSerializer, PlanCarSerializer


class CarView(generics.ListCreateAPIView):
    queryset = Car.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = CarSerializer


class PlanView(generics.ListCreateAPIView):
    queryset = Plan.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = PlanSerializer


class PlanCarView(generics.ListCreateAPIView):
    queryset = PlanCar.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = PlanCarSerializer
