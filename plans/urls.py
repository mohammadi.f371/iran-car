from django.urls import path

from plans.views import CarView, PlanView, PlanCarView

urlpatterns = [
    path('car/', CarView.as_view(), name='car-url'),
    path('plan/', PlanView.as_view(), name='plan-url'),
    path('plan_car/', PlanCarView.as_view(), name='plan_car-url'),
]
