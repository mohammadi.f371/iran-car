from rest_framework import serializers
from .models import Car, Plan


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('name',
                  'company',
                  'exterior_color',
                  'interior_color',
                  'code_product',
                  'model_product',
                  'fuel_type',
                  'features',
                  'account_type')
        extra_kwargs = {
        }


class PlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ('type_plan',
                  'deliver_time',
                  'insurance',
                  'name_insurance',
                  'name',
                  'date',
                  'expired')
        extra_kwargs = {
        }


class PlanCarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ('car',
                  'plan',
                  'cost',
                  'Amount_on_account',
                  'number')
        extra_kwargs = {
        }
