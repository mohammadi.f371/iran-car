from django.contrib import admin

from .models import Car, Plan, PlanCar, UserPlan
from django.contrib import messages
import random
from datetime import date, timedelta


class CarAdmin(admin.ModelAdmin):
    list_display = ['name', 'company', 'code_product', 'model_product', 'account_type']
    ordering = ['name']


class LotteryAdmin(admin.ModelAdmin):
    list_display = ['type_plan', 'name', 'date',  'lottery']
    ordering = ['date']
    actions = ['Lottery']
    readonly_fields = ('lottery',)

    @admin.action(description='Do the lottery')
    def Lottery(self, request, queryset):

        for obj in queryset:

            expired_date = obj.date + timedelta(obj.expired)
            if date.today() <= expired_date:
                messages.add_message(request, messages.ERROR, 'plan is active, you cant do this')
            elif obj.lottery:
                messages.add_message(request, messages.ERROR, 'The lottery has already been done')
            elif not obj.lottery:
                plan_car = PlanCar.objects.filter(plan=obj)
                for p in plan_car:
                    user = UserPlan.objects.filter(PlanCar=p)

                    if len(user) <= p.number:
                        UserPlan.objects.filter(pk__in=user).update(win=True)
                    else:
                        user_win = random.sample(list(user), p.number)

                        user_win_id = [w.id for w in user_win]

                        UserPlan.objects.filter(pk__in=user_win_id).update(win=True)
                messages.add_message(request, messages.INFO, 'Our lottery was done successfully')
                queryset.update(lottery=True)


class PlanCarAdmin(admin.ModelAdmin):
    list_display = ['car', 'plan', 'number']


class UserPlanAdmin(admin.ModelAdmin):
    list_display = ['user', 'PlanCar', 'win']
    ordering = ['-win']
    readonly_fields = ('win',)


admin.site.register(Car, CarAdmin)
admin.site.register(Plan, LotteryAdmin)
admin.site.register(PlanCar, PlanCarAdmin)
admin.site.register(UserPlan, UserPlanAdmin)
